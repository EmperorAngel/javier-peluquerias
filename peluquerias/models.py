from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
# Create your models here.


class Trabajador(models.Model):
    nombre = models.CharField(max_length=50, null=False)
    apellido = models.CharField(max_length=50,null=True)
    rut = models.CharField(max_length=14)
    telefono =  models.CharField(max_length=50, null=False)
    fechaingreso = models.DateField(null=True)
    rol=models.CharField(max_length=50, null=False)
    contrasenia = models.CharField(max_length=50)

    class Meta:

        permissions = (
            ('is_administrador',_('is administrador')),
            ('is_peluquero',_('is peluquero')),
            ('is_cajero',_('is cajero')),

        )


    def __str__(self):
        return self.nombre



class Trabajo(models.Model):
    tipotrabajo = models.CharField(max_length=50, null=True)
    precio = models.IntegerField(null=True)

    def __str__(self):
        return self.tipotrabajo

class Boleta(models.Model):

    nombrecliente = models.CharField(max_length=50, null=True)
    peluquero = models.ForeignKey(User,on_delete=models.CASCADE,null=True,related_name="peluquero")
    cajero = models.ForeignKey(User,on_delete=models.CASCADE,null=True,related_name="cajero")
    preciopagado = models.IntegerField(null=True)
    fecha = models.DateField(null=True)
    estadopago = models.BooleanField('Pagado',default=False)


