from django.apps import AppConfig


class PeluqueriasConfig(AppConfig):
    name = 'peluquerias'
