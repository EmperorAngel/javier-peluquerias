from django.shortcuts import render
from .models import Trabajador,Trabajo,Boleta
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout,login as auth_login
from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth.models import Permission
from django.core.exceptions import ObjectDoesNotExist
import datetime
from django.db.models import Sum

# Create your views here.

def index(request):
    return render(request,'index.html',{'nombre':'mario'})

def login(request):
    return render(request,'login.html',{})

def registro(request):
    return render(request,'registro.html',{})

def caja(request):
    user = request.user
    preciopagado__sum = Boleta.objects.filter(fecha__month=datetime.date.today().month,fecha__year=datetime.date.today().year).aggregate(Sum('preciopagado'))['preciopagado__sum']
    return render(request,'caja.html',{"USUARIO":user.first_name + " " + user.last_name,'boletas': Boleta.objects.filter(fecha__month=datetime.date.today().month,fecha__year=datetime.date.today().year),'total':preciopagado__sum})


def login_iniciar(request):
    username = request.POST.get('nombre','')
    contrasenia = request.POST.get('contrasenia','')
    user = authenticate(request,username=username,password=contrasenia)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("peluqueria")
    else:
        return redirect("registro")

@login_required(login_url='login')
def cerrar_session(request):
    del request.user
    logout(request)
    return redirect('index')

@login_required(login_url='login')
def peluqueria(request):
    usuario = request.session.get('usuario',None)
    user = request.user
    if user.has_perm('peluquerias.is_administrador'):

        return render(request,'peluqueria.html',{"USUARIO":user.first_name + " " + user.last_name,'cortes':Trabajo.objects.all()})

    elif user.has_perm('peluquerias.is_peluquero'):
        auth_login(request, user)
        return render(request,'peluqueria.html', {"USUARIO":user.first_name + " " + user.last_name})
    
    elif user.has_perm('peluquerias.is_cajero'):
        auth_login(request, user)
        return render(request,'peluqueria.html',{"USUARIO":user.first_name + " " + user.last_name})

    else:
        return render(request,'peluqueria.html')


def crearusuario(request):

    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    rut = request.POST.get('rut','')
    telefono = request.POST.get('telefono',0)
    fechaingreso = request.POST.get('fechaingreso','')
    rol = request.POST.get('rol','')
    contrasenia = request.POST.get('contrasenia','')

    trabajador = Trabajador(nombre=nombre,apellido=apellido,rut=rut,telefono=telefono
    ,fechaingreso=fechaingreso,rol=rol,contrasenia=contrasenia)


    if (rol=="A"):
        permiso = "is peluquero"
    else:
        permiso = "is cajero"


    permission = Permission.objects.get(name=permiso)
    p= User.objects.create_user(username=trabajador.rut,password=trabajador.contrasenia,first_name=trabajador.nombre,last_name=trabajador.apellido)
    p.save()
    #p = User.objects.get(username=persona.email)
    p.user_permissions.add(permission)
    trabajador.save()
    return redirect('login')


@login_required(login_url='login')
def crearTrabajo(request):
    tipotrabajo = request.POST.get('tipotrabajo','')
    precio = request.POST.get('precio','')
    t = Trabajo(tipotrabajo=tipotrabajo,precio=precio)
    t.save()
    return redirect('peluqueria')

@login_required(login_url='login')
def eliminarTrabajo(request,id):
    t = Trabajo.objects.get(pk = id)
    t.delete()
    return redirect('peluqueria')