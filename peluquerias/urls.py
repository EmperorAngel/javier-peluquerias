from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('login/iniciar',views.login_iniciar,name="iniciar"),
    path('login/registro/', views.registro, name='registro'),
    path('login/registro/crearusuario',views.crearusuario,name="crearusuario"),
    path('cerrarsession/',views.cerrar_session,name="cerrar_session"),
    path('peluqueria/', views.peluqueria, name='peluqueria'),
    path('registro/crearTrabajo',views.crearTrabajo,name="crearTrabajo"),
    path('registro/eliminarTrabajo/<int:id>',views.eliminarTrabajo,name="eliminarTrabajo"),
    path('peluqueria/caja', views.caja, name='caja'),



    
]