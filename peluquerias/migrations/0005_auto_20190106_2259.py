# Generated by Django 2.1.2 on 2019-01-07 01:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('peluquerias', '0004_auto_20190106_2041'),
    ]

    operations = [
        migrations.CreateModel(
            name='Boleta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombrecliente', models.CharField(max_length=50, null=True)),
                ('preciopagado', models.IntegerField(null=True)),
                ('fecha', models.DateTimeField(null=True)),
                ('estadopago', models.BooleanField(default=False, verbose_name='Pagado')),
                ('cajero', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cajero', to=settings.AUTH_USER_MODEL)),
                ('peluquero', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='peluquero', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Trabajo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipotrabajo', models.CharField(max_length=50, null=True)),
                ('precio', models.IntegerField(null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='RegistroCaja',
        ),
        migrations.DeleteModel(
            name='RegistroTrabajo',
        ),
    ]
