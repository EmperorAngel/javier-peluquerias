# Generated by Django 2.1.2 on 2019-01-06 23:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('peluquerias', '0002_trabajador_contrasenia'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='trabajador',
            options={'permissions': (('is_administrador', 'is administrador'), ('is_peluquero', 'is peluquero'), ('is_cajero', 'is cajero'))},
        ),
    ]
